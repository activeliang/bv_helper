# 使用

```
$bundle install # 安装依赖gems
$bin/start -h # 查看启动帮助

$bin/start -e development -D # 作为一个daemon以开发环境启动
$bin/stop # 关闭进程
$bin/console # 进入项目的console，方便测试
```

#### 文件

./run.log 日志文件 (只在deamon模式下)
.bv_helper.pid 进程PID
test


#### 调试websocket和EventMachine：

##### console版：
$ bin/console   #进入console
$ item = Ex.bitmex.new('xbtusd')
$ item.depth{|x| puts x }   #终端需要能科学上网
停止websocket的方法：`item.stop`


##### EM.run版：
$ bin/start   #启动服务
$  curl -H "Content-Type: application/json" \
    -X POST -d '{ "access_key": "access_key","secret_key": "secret_key", "nonce": "1500793319499", "method": "depth","params": {"symbol": "XBTUSD"}}' \
    http://127.0.0.1:2000/bitmex
