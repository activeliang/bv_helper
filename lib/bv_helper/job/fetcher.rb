require 'bv_helper/job/base'

module BvHelper
  module Job
    class Fetcher < Base

      def do_run
        sleep 1
        puts "Fetcher done!"
      end
    end

  end
end
