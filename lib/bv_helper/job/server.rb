require 'eventmachine'
require 'json'
module BvHelper
  module Job
    module Server
      class << self
        attr_accessor :fetching_pair, :depth_data, :request_time, :restart_at, :get_time
      end

      def post_init
        # puts "-- someone connected to the echo server!"
      end

      def receive_data data
        binding.pry
        process data
      end

      def process data
        # 检查交易所名称
        begin
          ex = data.scan(/\s\/\w+/).first.gsub(/\s\//, '')
        rescue
          ex = nil
        end
        # 如果是支持的交易所，进行捞数据
        support_ex = ['bitfinex', 'zb', 'poloniex', 'bitmex']
        if ex && support_ex.include?(ex)
          params = JSON.parse data.scan(/{.*/).first
          puts "=>>> request: \033[44;37m [#{ex}] \033[0m [#{params['params']['symbol']}] GetDepth", :notice
          response = find_data ex, params['params']['symbol']
          send_data res_data(response.to_json)
        else
          # 出错，关闭连接
          close_connection
          puts "Unsupported exchanges: #{ex}", :alert
        end
        close_connection if data =~ /quit/i
      end

      def unbind
        # puts "-- someone disconnected from the echo server!"
      end

      # 解析请求的json
      def render_request_body data
        params = data.scan(/{.*/).first
        params = JSON.parse(params)
      end

      # 进行捞数据
      def find_data(ex, symbol)
        $fetching_pair ||= {}
        $depth_data ||= {}
        $request_time ||= {}

        ex_pair = ex + symbol
        $request_time[ex_pair] = Time.now
        # 检查数据是否存在，是否过期
        if $fetching_pair[ex_pair] && !$fetching_pair[ex_pair].ws.closed && timeout_check(ex_pair) && !check_some_ex_restart_time(ex, symbol)
          $depth_data[ex_pair]
        else
          puts "undefined websocket#{ex}_#{symbol} or the data has expired !!!", :alert
          restart_socket(ex, symbol)
        end
      end

      # 进行重启websockets操作
      def restart_socket(ex, symbol)
        $restart_at ||= {}
        ex_pair = ex + symbol

        # 打印信息
        puts "restart websocket for #{ex}_#{symbol}", :notice
        puts "\033[45;37m #{$fetching_pair[ex_pair].stop if $fetching_pair[ex_pair]} \033[0m"

        # 配置好全局数据
        $depth_data[ex_pair] = nil
        $fetching_pair[ex_pair] = thead = Ex.method(ex).call.new symbol.to_s.gsub(/_/, '')
        $restart_at[ex_pair] = Time.now

        # 调用depth方法，实现websocket连接
        thead.depth do |data|
          $depth_data[ex_pair] = data
          $fetching_pair[ex_pair] = $fetching_pair[ex_pair]
        end

        # 循环，直到拿到数据
        sum = 0
        loop do
          # 如果接收到数据，停止循环并返回数据
          if $depth_data[ex_pair] && timeout_check(ex_pair)
            puts "get_data", :notice
            break $depth_data[ex_pair]
          else
            puts "there is no data，sleep 10ms!!!", :alert
            sleep(0.1)
            sum += 1
            # 选择性重启
            if sum % 80 == 0
              puts "been sleeping 40 times, Ready to reboot websocket for #{ex_pair}", :alert
              restart_socket(ex, symbol)
            else
              if sum < 200
                puts "No data found, continue to loop! ! ! ![ #{sum} ]"
                next
              else
                puts "\033[45;37m timeout, close connection!!! \033[0m"
                close_connection
              end
            end
          end
        end
      end

      # 组织要返回数据的格式 HTTP header
      def res_data data
        str = "HTTP/1.1 200 OK\r\n" +
                     "Content-Type: text/plain\r\n" +
                     "Content-Length: #{data.bytesize}\r\n" +
                     "Connection: close\r\n"
        str << "\r\n" << data << "\r\n"
      end

      # 检查数据是否过期，8秒为过期
      def timeout_check ex_pair
        $depth_data.any? && $depth_data[ex_pair] && $depth_data[ex_pair][:data] && $depth_data[ex_pair][:data][:time] > ((Time.now - 8).to_f * 1000).to_i
      end

      # 某些交易所需要自己维护订单薄，这类的设置为半小时自动reset。
      def check_some_ex_restart_time(ex, symbol)
        if ex == "bitfinex" || "bitmex"
          if !$restart_at[ex + symbol].to_s.empty? && Time.now - $restart_at[ex + symbol] > 1800 || $restart_at[ex + symbol].to_s.empty?
            puts "\033[47;30m Maintenance orderbook for more than 30 minutes, automatic restart of websockets #{ex} \033[0m"
            return true
          end
        end
      end

      # 设置别名，优化puts方法
      alias :old_puts :puts
      def puts txt, mode=nil
        case mode
        when :alert
          old_puts "\033[41;37m #{Time.now.strftime("%F %T")} #{txt} \033[0m "
        when :notice
          old_puts "\033[32m #{Time.now.strftime("%F %T")} #{txt} \033[0m"
        else
          old_puts "#{Time.now.strftime("%F %T")} #{txt}"
        end
      end
    end
  end
end
