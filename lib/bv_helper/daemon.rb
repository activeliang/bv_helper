module BvHelper
  module Daemon

    class << self
      attr_accessor :logger
      attr_accessor :log_file
    end

    def self.run config, run_as_daemon = false
      setup_logger(run_as_daemon)

      EM.run do
        puts "Start process.."
        puts "PID: #{Process.pid}"

        EventMachine.start_server "127.0.0.1", 2000, BvHelper::Job::Server

        EM.add_periodic_timer(120) do
          # 检查超过2分钟没有再请求的交易对，关闭其websocket
          puts "\033[45;37m check the global websocket process~ \033[0m"
          $request_time ||= nil
          $fetching_pair ||= nil
          if $request_time && $fetching_pair
            $request_time.each do |ex_pair, time|
              if time < Time.now - 120
                $fetching_pair[ex_pair].stop
                puts "\033[31m No request for more than one minute, kill process for #{ex_pair}. \033[0m"
              end
            end
          end
        end

        EM.add_periodic_timer 5 do
          puts "\033[42;30m The program is running... \033[0m"
        end

        handle_signal
      end
    end

    protected
    def self.setup_logger run_as_daemon
      self.log_file = File.expand_path('../../../run.log', __FILE__)
      FileUtils.touch(self.log_file) unless File.exists?(self.log_file)
      self.logger = Logger.new(self.log_file)

      if run_as_daemon
        $stdout.reopen(self.log_file, "a+")
        $stdout.sync = true
        $stderr.reopen($stdout)
      end
    end

    def self.handle_signal
      [:INT, :QUIT, :TERM].each do |sig|
        trap(sig) do
          # clear pid file
          pid_file = File.expand_path('../../..bv_helper.pid', __FILE__)
          FileUtils.rm_f(pid_file) if File.exists?(pid_file)

          puts "Exiting...."
          # 退出时，关闭所有websocket线程
          sum = 0
          $fetching_pair.each do |ex, item|
            sum += 1
            puts "Closed thread--#{sum}--#{ex}---#{item.stop}"
          end
          puts "\033[41;37m #{sig} Program exited!!!!! \033[0m"
          EM.stop
        end
      end
    end

  end

end
