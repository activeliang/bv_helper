require 'ex_base'
require 'api_demo/huobi'
class ExHitbtc < ExBase
  attr_accessor :bid_hash, :ask_hash

  def depth
    # 判断ws是否已经存在，先断开再连接，已免连接过多失控
    self.stop if self.ws

    @ws = ws = WebSocket::Client::Simple.connect "wss://api.hitbtc.com/api/2/ws"
    this = self

    # init connect...
    params = {"method": "subscribeOrderbook","params": { "symbol": @pair }, "id": 123 }
    ws.on :open do
      ws.send params.to_json
    end

    # 接收
    ws.on :message do |msg|
      data = JSON.parse(msg.data)

      # 维护orderbook
      # 首次初始化订单, hitbtc的orderbook数量很夸张，>>>> 初始化成功，bid数量：267, ask数量： 984 <<<<
      if data["method"] == "snapshotOrderbook"
        # 由于数据过多，这里只提取前50条数据
        this.bid_hash = data["params"]["bid"][0..49].map{|x| x.values}.to_h
        this.ask_hash = data["params"]["ask"][0..49].map{|x| x.values}.to_h
        # puts "----------------------->>>> 初始化成功，bid数量：#{this.bid_hash.count}, ask数量： #{this.ask_hash.count} <<<<--------------------------"
      end

      # 其后更新维护
      if data["method"] == "updateOrderbook"
        # 更新bid
        unless data["params"]["bid"].empty?
          # sum = []
          # sum_1 = this.bid_hash.count
          data["params"]["bid"].each do |item|
            if item["size"].to_d == 0
              # binding.pry
              this.bid_hash.delete item['price']
              # sum[0] = sum[0].to_i + 1
            else
              # binding.pry
              this.bid_hash[item["price"]] = item["size"]
              # sum[1] = sum[1].to_i + 1
            end
          end
          # puts "*******************************>>> 更新bid, before_count: #{sum_1},  数量变化： #{this.bid_hash.count - sum_1} , after_count: #{this.bid_hash.count}   <<<**********************************"
        end

        # 更新ask
        unless data["params"]["ask"].empty?
          # sum = []
          # sum_1 = this.ask_hash.count
          data["params"]["ask"].each do |item|
            if item["size"].to_d == 0
              this.ask_hash.delete item['price']
              # sum[0] = sum[0].to_i + 1
            else
              this.ask_hash[item["price"]] = item["size"]
              # sum[1] = sum[1].to_i + 1
            end
          end
          # puts "*******************************>>> 更新ask, before_count: #{sum_1},  数量变化： #{this.ask_hash.count - sum_1} , after_count: #{this.bid_hash.count}   <<<**********************************"
        end
      end

      # binding.pry
      # 赋值给实例变量
      this.bids = bids = this.to_hash this.bid_hash.to_a.map{|x| x.map{|x| x.to_d}}.sort.reverse
      this.asks = asks = this.to_hash this.ask_hash.to_a.map{|x| x.map{|x| x.to_d}}.sort
      current_time = (Time.now.to_f * 1000).to_i
      this.updated_at = current_time
      this.get_depth = { bids: bids, asks: asks, updated_at: current_time }

      # puts "-------------------------------------------------------"
      # puts "-----------------------#{bid.count}-------------------------"
      # puts "-----------------------#{ask.count}-------------------------"
      # puts "-------------------------------------------------------"

      # 执行block
      res = { get_depth: { bids: bids, asks: asks }, updated_at: current_time }
      yield res
    end
  end

  def get_account(pair=nil)
    begin
      endpoint = '/trading/balance'
      res = utils endpoint, 'get'

      # 提取当前pair
      current_pair = nil
      res.map{ |x| current_pair = x if x['currency'] == (pair && pair.upcase || currency) }
      if current_pair.present?
        {
          status: 'ok',
          frozen_balance: current_pair['reserved'].to_d,
          stocks: current_pair['available'].to_d,
          balance: current_pair['reserved'].to_d + current_pair['available'].to_d,
          frozen_stocks: nil
        }
      else
        { status: 'failed', message: res }
      end
    rescue Exception => e
      e.message
    end
  end

  def sell(price, amount)
    begin
      endpoint = '/order'
      params = {symbol: @pair, side: 'sell', quantity: amount, price: price }
      res = utils endpoint, 'post', params
      if res['id'].present?
        { status: 'ok', order_id: res['clientOrderId'] }
      else
        { status: 'failed', message: res }
      end
    rescue Exception => e
      { stauts: 'failed', message: e.message }
    end
  end

  def buy(price, amount)
    begin
      endpoint = '/order'
      params = {symbol: @pair, side: 'buy', quantity: amount, price: price }
      res = utils endpoint, 'post', params
      if res['id'].present?
        { status: 'ok', order_id: res['clientOrderId'] }
      else
        { status: 'failed', message: res }
      end
    rescue Exception => e
      { stauts: 'failed', message: e.message }
    end
  end

  def get_order(order_id)
    begin
      endpoint = "/order/#{order_id}"
      res = utils endpoint, 'get'
      if res['id'].present?
        {
          status: 'ok',
          order_id: res['clientOrderId'],
          price: res['price'].to_d,
          amount: res['quantity'].to_d,
          deal_amount: res['cumQuantity'].to_d,
          is_cancelled: res['status'] == 'new' ? false : true
        }
      else
        { status: "failed", message: res }
      end
    rescue Exception => e
      { stauts: 'failed', message: e.message }
    end
  end

  def cancel_order(order_id)
    begin
      endpoint = "/order/#{order_id}"
      res = utils endpoint, 'delete'
      if res['id'].present?
        {
          status: 'ok',
          order_id: res['clientOrderId']
        }
      else
        { status: "failed", message: res }
      end
    rescue Exception => e
      { stauts: 'failed', message: e.message }
    end
  end

  private
    def utils endpoint, method, params={}
      url = "https://#{ENV['hitbtc_ak']}:#{ENV['hitbtc_sk']}@api.hitbtc.com/api/2" + endpoint
      puts url
      if method == 'get'
        res = RestClient.get url, params
      elsif method == 'post'
        res = RestClient.post url, params
      else
        res = RestClient.delete url
      end

      JSON.parse res
    end
end
