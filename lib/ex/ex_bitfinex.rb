require 'ex_base'
class ExBitfinex < ExBase
  attr_accessor :bid_hash, :ask_hash

  def initialize(pair)
    raise ArgumentError, "没有传递参数pair" unless pair
    @client = Bitfinex::Client.new
    @pair = pair
    @exchange_name = "Bitfinex"
  end

  def depth

    @bid_hash = Hash.new
    @ask_hash = Hash.new

    # 判断ws是否已经存在，先断开再连接，已免连接过多失控
    self.stop if self.ws

    @ws = ws = WebSocket::Client::Simple.connect 'wss://api.bitfinex.com/ws'
    params = { "event": "subscribe", "channel": "book", "prec": "R0", "pair": @pair }
    this = self
    # 发送
    ws.on :open do
      ws.send params.to_json
    end

    # 接收信息后 do something...
    ws.on :message do |msg|
      raw_data = JSON.parse msg.data

      # puts msg.data
      # puts "-----------------#{raw_data}------------------------------"
      # 处理raw_book第一次发来的50个订单薄
      if this.bid_hash.empty? && this.ask_hash.empty?
        if raw_data.class == Array && raw_data[1].count == 50
          raw_data[1].each do |item|
            if item[2] > 0
              this.bid_hash[item[0].to_s] = [item[1], item[2]]
            else
              this.ask_hash[item[0].to_s] = [item[1], item[2].abs]
            end
          end
        end
      end

      # 其后第次更新数据
      if raw_data.class == Array && raw_data.count == 4
        # binding.pry
        if raw_data[3] > 0
          if raw_data[2] == 0
            this.bid_hash.delete raw_data[1].to_s
          else
            this.bid_hash[raw_data[1].to_s] = [raw_data[2], raw_data[3]]
          end
          # puts "------- 判断正负数： #{raw_data[3]}, 判断价格是否为0： #{raw_data[2]}, 最后数量： #{this.bid_hash.count}   --------------------"
        else
          if raw_data[2] == 0
            this.ask_hash.delete raw_data[1].to_s
          else
            this.ask_hash[raw_data[1].to_s] = [raw_data[2], raw_data[3].abs]
          end
          # puts "------- 判断正负数： #{raw_data[3]}, 判断价格是否为0： #{raw_data[2]}, 最后数量： #{this.ask_hash.count}   --------------------"
        end
      end

      # 检查orderbook的数量变化，应该保持在25上下才对
      # puts "*********************>>> #{this.bid_hash.count} <<<************************"
      # puts "*********************>>> #{this.ask_hash.count} <<<************************"


      bids = this.bid_hash.values.sort!.reverse!
      asks = this.ask_hash.values.sort!

      # 打印出来检查
      # puts "------------------------¥¥¥¥¥¥¥¥---------------------------"
      #   puts bid.join(",")
      #   puts "@@@@@@@@@@@@@@@"
      #   puts ask.join(",")
      # puts "------------------------¥¥¥¥¥¥¥¥---------------------------"

      if bids.any? && asks.any?
        # 赋值给实例变量
        this.bids = bids
        this.asks = asks
        current_time = (Time.now.to_f * 1000).to_i
        this.updated_at = current_time
        res = { data: { bids: bids, asks: asks, time: current_time }, updated_at: current_time }
        this.get_depth = res
        # 执行block
        yield res
      else
        return
      end

    end
  end

  def get_account(pair=nil)
    res = @client.balances
    # 提取当前pair
    current_pair = nil
    res.map{|x| current_pair = x if x['currency'] == (pair || currency.downcase) && x['type'] == 'exchange'}

    if current_pair.present?
      {
        status: 'ok',
        frozen_balance: current_pair['amount'].to_d - current_pair['available'].to_d,
        stocks: current_pair['available'].to_d,
        balance: current_pair['amount'].to_d,
        frozen_stocks: nil
      }
    else
      { status: 'failed', message: res }
    end
  end

  def buy(price, amount)
    begin
      res = @client.new_order(@pair, amount, 'limit', 'buy', price)
      if res['order_id'].present?
        { stauts: 'ok', order_id: res['order_id'] }
      else
        { status: 'failed', message: res }
      end
    rescue Exception => e
      { stauts: 'failed', message: e.message }
    end
  end

  def sell(price, amount)
    begin
      res = @client.new_order(@pair, amount, 'limit', 'sell', price)
      if res['order_id'].present?
        { stauts: 'ok', order_id: res['order_id'] }
      else
        { status: 'failed', message: res }
      end
    rescue Exception => e
      { stauts: 'failed', message: e.message }
    end
  end

  def get_order(order_id)
    begin
      res = @client.order_status(order_id)
      if res['id'].present?
        {
          status: 'ok',
          order_id: res['id'],
          price: res['price'],
          amount: res['original_amount'],
          deal_amount: res['executed_amount'],
          is_cancelled: res['is_cancelled']
        }
      else
        { status: "failed", message: res }
      end
    rescue Exception => e
      { stauts: 'failed', message: e.message }
    end
  end

  def cancel_order(order_id)
    begin
      res = @client.cancel_orders(order_id)
      if res['id'].present?
        { status: 'ok', order_id: res['id'] }
      else
        { status: "failed", message: res }
      end
    rescue Exception => e
      { stauts: 'failed', message: e.message }
    end
  end

end
