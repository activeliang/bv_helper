require 'ex_base'
class ExZb < ExBase

  def depth
    # 判断ws是否已经存在，先断开再连接，已免连接过多失控
    stop if self.ws

    @ws = ws = WebSocket::Client::Simple.connect "wss://api.zb.com:9999/websocket"
    this = self

    # init connect...
    params = { 'event': 'addChannel', 'channel': "#{@pair}_depth" }
    ws.on :open do
      ws.send params.to_json
    end

    # 接收
    ws.on :message do |msg|
      # puts msg.data
      # binding.pry
      data = JSON.parse msg.data
      bids = data['bids']
      asks = data['asks'].reverse!

      if bids.any? && asks.any?
        # 赋值给实例变量
        this.bids = bids
        this.asks = asks
        current_time = (Time.now.to_f * 1000).to_i
        this.updated_at = current_time
        res = { data: { bids: bids, asks: asks, time: current_time }, updated_at: current_time }
        this.get_depth = res
        # 执行block
        yield res
      else
        return
      end
    end
  end

  def get_account(pair=nil)
    endpoint = '/getAccountInfo'
    params = { 'method': 'getAccountInfo' }
    res = utils endpoint, params

    # 提取当前pair的余额
    current_pair = nil
    res['result']['coins'].map{ |x| current_pair = x if x['enName'] == (pair && pair.upcase || currency) }
    if current_pair.present?
      {
        status: 'ok',
        frozen_balance: current_pair['freez'].to_d,
        stocks: current_pair['available'].to_d,
        balance: current_pair['freez'].to_d + current_pair['available'].to_d,
        frozen_stocks: nil
      }
    else
      { status: 'failed', message: res }
    end
  end

  def sell(price, amount)
    endpoint = '/order'
    params = { 'amount': amount,
               'currency': @pair,
               'method': 'order',
               'price': price,
               'tradeType': 0 }
    res = utils endpoint, params

    # 检查是否操作成功
    if res['code'] == 1000
      id = res['id']
      { status: 'ok', order_id: id }
    else
      { status: "failed", message: res }
    end
  end

  def buy(price, amount)
    endpoint = '/order'
    params = { 'amount': amount,
               'currency': @pair,
               'method': 'order',
               'price': price,
               'tradeType': 1 }
    res = utils endpoint, params

    # 检查是否操作成功
    if res['code'] == 1000
      id = res['id']
      { status: 'ok', order_id: id }
    else
      { status: "failed", message: res }
    end
  end

  def get_order(order_id)
    endpoint = '/getOrder'
    params = { 'currency': @pair,
               'id': order_id,
               'method': 'getOrder' }
    res = utils endpoint, params
    # puts res

    # 检查操作是否成功
    if !res['code'].present?
      {
        stauts: 'ok',
        order_id: res['id'],
        price: res['price'],
        amount: res['total_amount'],
        deal_amount: res['trade_amount'],
        is_cancelled: res['status'] == 1 ? true : false
      }
    else
      { status: 'failed', message: res }
    end
  end

  def cancel_order(order_id)
    endpoint = '/cancelOrder'
    params = { 'currency': @pair,
               'id': order_id,
               'method': 'cancelOrder'  }
    res = utils endpoint, params

    # 检查操作状态
    if res['code'] == 1000
      { status: 'ok', order_id: order_id }
    else
      { status: 'failed', message: res }
    end
  end

  private

    def sha1(str)
      Digest::SHA1.hexdigest(str)
    end

    def hmac_md5(ssk, params)
      OpenSSL::HMAC.hexdigest "md5", ssk, params.to_query
    end

    def complete_uri(endpoint, params_query, sign)
      current_time = (Time.now.to_f * 1000).to_i
      host = 'https://trade.zb.com/api'
      host + endpoint + '?' + params_query + "&sign=#{sign}&reqTime=#{current_time}"
    end

    def build_query(endpoint, params)
      params = { 'accesskey': ENV['zb_ak'] }.merge params
      ssk = sha1 ENV["zb_sk"]
      sign = hmac_md5 ssk, params
      complete_uri endpoint, params.to_query, sign
    end

    def http_res uri
      res = RestClient.get uri
      data = JSON.parse res.body
    end

    def utils endpoint, params={}
      uri = build_query endpoint, params
      res = http_res uri
    end

end
