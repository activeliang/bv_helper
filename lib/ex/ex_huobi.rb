require 'ex_base'
require 'json'
require 'zlib'
class ExHuobi < ExBase

  def initialize pair
    @pair = pair
    @client = Huobi.new
    @exchange_name = "Huobi"
  end

  def depth
    # 判断ws是否已经存在，先断开再连接，已免连接过多失控
    stop if self.ws

    @ws = ws = WebSocket::Client::Simple.connect "wss://api.huobipro.com/ws"
    this = self

    # init connect...
    params = { "sub": "market.#{@pair}.depth.step0", "id": "id1" }
    ws.on :open do
      ws.send params.to_json
    end

    # 接收
    ws.on :message do |msg|
      # puts "拿到数据啦：#{msg}"
      data = JSON.parse Zlib::GzipReader.new(StringIO.new(msg.data)).read

      # 保持与服务器联系
      if data['ping'].present?
        pong = { 'pong': data['ping'].to_i }
        ws.send pong.to_json
      else
        bids = this.to_hash data['tick']['bids']
        asks = this.to_hash data['tick']['asks']

        # # 赋值给实例变量
        this.bids = bids
        this.asks = asks
        current_time = (Time.now.to_f * 1000).to_i
        this.updated_at = current_time
        this.get_depth = { bids: bids, asks: asks, updated_at: current_time }

        # # 执行block
        res = { get_depth: { bids: bids, asks: asks }, updated_at: current_time }
        yield res
      end
    end
  end

  def get_account(pair=nil)
    res = @client.balances
    # 提取当前pair的余额
    current_pair = {}
    res['data']['list'].each do |x|
      current_pair['stocks'] = x['balance'] if x['currency'] == (pair || currency.downcase) && x['type'] == 'trade'
      current_pair['frozen'] = x['balance'] if x['currency'] == (pair || currency.downcase) && x['type'] == 'frozen'
    end
    if current_pair.present?
      {
        status: 'ok',
        frozen_balance: current_pair['frozen'].to_d,
        stocks: current_pair['stocks'].to_d,
        balance: current_pair['frozen'].to_d + current_pair['stocks'].to_d,
        frozen_stocks: nil
      }
    else
      { status: 'failed', message: res }
    end
  end

  def buy(price, amount)
    @client.new_order(@pair, 'buy', price, amount)
    if res['status'] == 'ok'
      { status: 'ok', order_id: res['data'] }
    else
      { status: 'failed', message: res['err-msg'] }
    end
  end

  def sell(price, amount)
    res = @client.new_order(@pair, 'sell', price, amount)
    if res['status'] == 'ok'
      { status: 'ok', order_id: res['data'] }
    else
      { status: 'failed', message: res['err-msg'] }
    end
  end

  def get_order(order_id)
    res = @client.order_status(order_id)
    if res['status'] == 'ok'
      {
        status: 'ok',
        order_id: res['data']['id'],
        price: res['data']['price'],
        amount: res['data']['amount'],
        deal_amount: res['data']['field-amount']
      }
    else
      { stauts: 'failed', message: res['err-msg'] }
    end
  end

  def cancel_order(order_id)
    res = @client.submitcancel(order_id)
    if res['status'] == 'ok'
      { status: 'ok', order_id: res['data'] }
    else
      { status: 'failed', message: res['err-msg'] }
    end
  end

end
