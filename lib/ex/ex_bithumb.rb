require 'ex_base'
class ExBithumb < ExBase

  def initialize(pair)
    raise ArgumentError, "没有传递参数pair" unless pair
    @pair = pair
    @exchange_name = "Bithumb"
    $ex_bithumb_depth_data ||= {}
    $ex_bithumb_depth_data['pairs'] ||= []
    $ex_bithumb_depth_data['pairs'] << pair

    # 创建多线程并行的行情API，10次/秒
    if !$ex_bithumb_thread.present? || $ex_bithumb_thread.present? && !$ex_bithumb_thread.status
      $ex_bithumb_thread = Thread.new do
        loop do
          Thread.new{
            if $ex_bithumb_depth_data['pairs'].any?
              current_time = (Time.now.to_f * 1000).to_i
              $ex_bithumb_depth_data['data'] = JSON.parse(RestClient.get('https://api.bithumb.com/public/orderbook/ALL').body)
              $ex_bithumb_depth_data['updated_at'] = current_time
            else
              $ex_bithumb_thread.kill
            end
          }
          sleep(0.1)
        end
      end
    end
  end

  def depth
  end

  def bids
    res = $ex_bithumb_depth_data
    if res['data']['status'] == '0000'
      refactor_format res['data']['data'][@pair.upcase]['bids']
    end
  end

  def asks
    res = $ex_bithumb_depth_data
    if res['data']['status'] == '0000'
      refactor_format res['data']['data'][@pair.upcase]['asks']
    end
  end

  def get_depth
    { bids: bids, asks: asks, updated_at: updated_at }
  end

  def updated_at
    res = $ex_bithumb_depth_data['updated_at']
  end

  def stop
    $ex_bithumb_depth_data['pairs'].delete(@pair)
  end

  # 以下是交易API
  def get_account(pair=@pair)
    end_point = '/info/balance'
    params = {
      "currency": pair.upcase,
    }
    res = utils end_point, params
    if res['status'] == '0000'
      {
        status: 'ok',
        frozen_balance: res['data']["in_use_#{pair}"].to_d,
        stocks: res['data']["available_#{pair}"].to_d,
        balance: res['data']["total_#{pair}"].to_d,
        frozen_stocks: nil
      }
    else
      { status: 'failed', message: res }
    end
  end

  def sell(price, amount)
    end_point = '/trade/place'
    params = {
      "order_currency": @pair.upcase,
      "Payment_currency": "KRW",
      "units": amount.to_s,
      "price": price.to_s,
      "type": "ask"
    }
    res = utils end_point, params

    if res['status'] == '0000'
      { status: 'ok', order_id: res['order_id'] }
    else
      { status: 'failed', message: res }
    end
  end

  def buy(price, amount)
    end_point = '/trade/place'
    params = {
      "order_currency": @pair.upcase,
      "Payment_currency": "KRW",
      "units": amount.to_s,
      "price": price.to_s,
      "type": "bid"
    }
    res = utils end_point, params

    if res['status'] == '0000'
      { status: 'ok', order_id: res['order_id'] }
    else
      { status: 'failed', message: res }
    end
  end

  def get_order(order_id, type)
    res_1 = get_order_1(order_id, type)
    # binding.pry
    if res_1['status'] == '0000'
      {
        status: 'ok',
        order_id: res_1['data'][0]['order_id'],
        price: res_1['data'][0]['price'].to_d,
        amount: res_1['data'][0]['units'].to_d,
        deal_amount: res_1['data'][0]['units'].to_d - res_1['data'][0]['units_remaining'].to_d,
        is_cancelled: false
      }
    else
      # 如果在时行中的订单找不到该订单号，那去已完成的订单里找
      res_2 = get_order_2(order_id, type)
      if res_2['status'] == '0000'
        amount_sum = 0
        res_2['data'].map{|x| amount_sum += x['units_traded'].to_d }
        {
          status: 'ok',
          order_id: order_id.to_d,
          price: res_2['data'][0]['price'].to_i,
          amount: amount_sum,
          deal_amount: amount_sum,
          is_cancelled: false
        }
      else
        # 此时如果还找不到，则订单已关闭或者不存在。
        { status: 'failed', message: "这个订单已关闭或者不存在。"}
      end
    end
  end

  def get_order_1(order_id, type)
    end_point = '/info/orders'
    params = {
      'order_id': order_id.to_s,
      'type': type,
      'currency': @pair.upcase
    }
    res = utils end_point, params
  end

  def get_order_2(order_id, type)
    end_point = '/info/order_detail'
    params = {
      'order_id': order_id.to_s,
      'type': type,
      'currency': @pair.upcase
    }
    res = utils end_point, params
  end

  def cancel_order(order_id, type)
    end_point = '/trade/cancel'
    params = {
      'type': type,
      'order_id': order_id.to_s,
      'currency': @pair.upcase
    }
    res = utils end_point, params

    if res['status'] == '0000'
      { status: 'ok', order_id: order_id }
    else
      { status: 'failed', message: res }
    end
  end

  def market_sell(amount)
    end_point = '/trade/market_sell'
    params = {
      'units': amount.to_s,
      'currency': @pair.upcase
    }
    res = utils end_point, params
  end

  def market_buy(amount)
    end_point = '/trade/market_sell'
    params = {
      'units': amount.to_s
    }
    res = utils end_point, params
  end

  private
    def refactor_format(data)
      new_array = []
      data.each do |h|
        new_array << { price: h['price'], amount: h['quantity'] }
      end
      new_array
    end

    def sign_params(pending_sign)
      sign = Base64.strict_encode64(OpenSSL::HMAC.hexdigest(OpenSSL::Digest.new("sha512"), ENV["bithumb_sk"], pending_sign))
    end

    def to_query hash
      hash.map{|k, v| hash[k] = URI.encode URI.encode(v), '/'}
      hash.to_a.map{|x| x.join('=')}.join('&')
    end

    def utils end_point, params
      uri = "https://api.bithumb.com" + end_point
      nNonce = (Time.now.to_f * 1000).to_i
      pending_sign = end_point + 0.chr + to_query(params) + 0.chr + nNonce.to_s
      sign = sign_params(pending_sign)
      header = {
        'Api-Key': ENV["bithumb_ak"],
        'Api-Sign': sign,
        'Api-Nonce': nNonce
      }

      res = RestClient.post uri, params, header
      JSON.parse res
    end
end
