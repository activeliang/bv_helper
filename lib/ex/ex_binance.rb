require 'ex_base'
class ExBinance < ExBase

  def initialize(pair)
    raise ArgumentError, "没有传递参数pair" unless pair
    @client = Binance::Client::REST.new api_key: ENV['binance_ak'], secret_key: ENV['binance_sk']
    @pair = pair
    @exchange_name = "Binance"
  end

  def depth
    # 判断ws是否已经存在，先断开再连接，已免连接过多失控
    self.stop if self.ws

    @ws = ws = WebSocket::Client::Simple.connect "wss://stream.binance.com:9443/ws/#{@pair}@depth20"
    this = self

    # 接收
    ws.on :message do |msg|
      data = JSON.parse msg.data
   # puts data
      if data["bids"] || data["asks"]
        # binding.pry
        bids = this.to_hash data['bids'].map{|x| x.pop; x.map{|x| x.to_d}}
        asks = this.to_hash data['asks'].map{|x| x.pop; x.map{|x| x.to_d}}

      else
        bid = nil
        ask = nil
      end

      # 赋值给实例变量
      this.bids = bids
      this.asks = asks
      current_time = (Time.now.to_f * 1000).to_i
      this.updated_at = current_time
      this.get_depth = { bids: bids, asks: asks, updated_at: current_time }

      # 执行block
      res = { get_depth: { bids: bids, asks: asks }, updated_at: current_time }
      yield res
    end
  end

  def get_account(pair=nil)
    res = @client.account_info

    # 提取当前pair的余额
    current_pair = nil
    # binding.pry
    res['balances'].map{ |x| current_pair = x if x['asset'] == (pair && pair.upcase || currency) }
    if current_pair.present?
      {
        status: 'ok',
        frozen_balance: current_pair['locked'].to_d,
        stocks: current_pair['free'].to_d,
        balance: current_pair['locked'].to_d + current_pair['free'].to_d,
        frozen_stocks: nil
      }
    else
      { status: 'failed', message: res }
    end
  end

  def buy(price, amount, type = "LIMIT")
    # 检查参数
    # TODO: 这里后续需要做一个判断，判断交易量是否达到币安最低交易量的标准
    res = @client.create_order! symbol: @pair.upcase, side: 'BUY', type: "LIMIT", timeInForce: 'GTC', quantity: amount, price: price
    if res['orderId'].present?
      { status: 'ok', order_id: res['orderId'] }
    else
      { status: 'failed', message: res }
    end
  end

  def sell(price, amount, type = "LIMIT")
    # 检查参数
    res = @client.create_order! symbol: @pair.upcase, side: 'SELL', type: "LIMIT", timeInForce: 'GTC', quantity: amount, price: price
    if res['orderId'].present?
      { status: 'ok', order_id: res['orderId'] }
    else
      { status: 'failed', message: res }
    end
  end

  def get_order(order_id)
    res = @client.query_order orderId: order_id, symbol: @pair.upcase
    if res['orderId'].present?
      {
        status: 'ok',
        order_id: res['orderId'],
        price: res['price'].to_d,
        amount: res['origQty'].to_d,
        deal_amount: res['executedQty'].to_d,
        is_cancelled: res['isWorking'] ? false : true
      }
    else
      { status: 'failed', message: res }
    end
  end

  def cancel_order(order_id)
    res = client.cancel_order! symbol: @pair.upcase, orderId: order_id
    if res['clientOrderId'].present?
      { status: 'ok', order_id: res['21407539'] }
    else
      { status: 'failed', message: res }
    end
  end

  # def exchange_name
  #   return "Binance"
  # end

end
