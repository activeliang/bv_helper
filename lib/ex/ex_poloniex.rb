require 'json'
require 'ex_base'
class ExPoloniex < ExBase
  attr_accessor :bid_hash, :ask_hash

  def depth
    # 判断ws是否已经存在，先断开再连接，已免连接过多失控
    self.stop if self.ws

    @ws = ws = WebSocket::Client::Simple.connect "wss://api2.poloniex.com/"
    this = self

    # init connect...
    # p网的pair默认基础币在前面，这里做下处理
    quote_pair = @pair.scan(/(btc|eth|usdt|xmr)\z/).first.first
    current_pair = @pair.gsub(quote_pair, '')
    params = {"command":"subscribe","channel": [quote_pair, current_pair].join('_').upcase }
    ws.on :open do
      ws.send params.to_json
    end

    # 接收
    ws.on :message do |msg|
      data = JSON.parse(msg.data)
      # puts data

      # 维护orderbook
      # 首次初始化订单, poloniex的orderbook数量很夸张，>>>> 初始化成功，bid数量：267, ask数量： 984 <<<<
      if data.count == 3 && data[2][0][0] == 'i'
        # 由于数据过多，这里只提取前50条数据
        this.bid_hash = data[2][0][1]['orderBook'][1].to_a[0..49].to_h
        this.ask_hash = data[2][0][1]['orderBook'][0].to_a[0..49].to_h
        # puts "----------------------->>>> 初始化成功，bid数量：#{this.bid_hash.count}, ask数量： #{this.ask_hash.count} <<<<--------------------------"
      end

      # 其后更新维护
      if data.count == 3 && data[2][0][0] == 'o'
        data[2].each do |item|
            # puts "检测2："
          if item[1] == 1
            # 更新bids
            if item[3] == '0.00000000'
              this.bid_hash.delete item[2]
            else
              this.bid_hash[item[2]] = item[3]
            end

          elsif item[1] == 0
            # 更新asks
            if item[3] == '0.00000000'
              this.ask_hash.delete item[2]
            else
              this.ask_hash[item[2]] = item[3]
            end

          end
        end
        # puts "*******************************>>> 更新bid, before_count: #{sum_1},  数量变化： #{this.bid_hash.count - sum_1} , after_count: #{this.bid_hash.count}   <<<**********************************"
        # puts "*******************************>>> 更新ask, before_count: #{sum_1},  数量变化： #{this.ask_hash.count - sum_1} , after_count: #{this.bid_hash.count}   <<<**********************************"
      end

      # 赋值给实例变量
      this.bids = bids = this.bid_hash.sort.reverse[0..49]
      this.asks = asks = this.ask_hash.sort[0..49]


      if bids.any? && asks.any?
        # 赋值给实例变量
        this.bids = bids
        this.asks = asks
        current_time = (Time.now.to_f * 1000).to_i
        this.updated_at = current_time
        res = { data: { bids: bids, asks: asks, time: current_time }, updated_at: current_time }
        this.get_depth = res
        # 执行block
        yield res
      else
        return
      end

    end
  end
end
