require 'ex_base'
require 'api_demo/huobi'
class ExBitmex < ExBase
  attr_accessor :bid_hash, :ask_hash

  def depth
    # 判断ws是否已经存在，先断开再连接，已免连接过多失控
    self.stop if self.ws

    @bid_hash ||= Hash.new
    @ask_hash ||= Hash.new

    Thread.new{
      EM.run{
        @ws = ws = Faye::WebSocket::Client.new "wss://www.bitmex.com/realtime"
        this = self

        # init connect...
        ws.on :open do
          params = {"op": "subscribe", "args": ["orderBookL2:#{@pair.upcase}"]}
          ws.send params.to_json
        end

        # 接收
        ws.on :message do |msg|
          data = JSON.parse(msg.data)

          if data['action']
            data['data'].each do |item|
              if item['side'] == 'Sell'
                case data['action']
                when 'partial'
                  this.ask_hash[item['id'].to_s] = [item['price'], item['size']]
                when 'insert'
                  this.ask_hash[item['id'].to_s] = [item['price'], item['size']]
                when 'update'
                  this.ask_hash[item['id'].to_s][1] = item['size']
                when 'delete'
                  this.ask_hash.delete item['id'].to_s
                end
              elsif item['side'] == 'Buy'
                case data['action']
                when 'partial'
                  this.bid_hash[item['id'].to_s] = [item['price'], item['size']]
                when 'insert'
                  this.bid_hash[item['id'].to_s] = [item['price'], item['size']]
                when 'update'
                  this.bid_hash[item['id'].to_s][1] = item['size']
                when 'delete'
                  this.bid_hash.delete item['id'].to_s
                end
              end
            end
          end
          # 维护orderbook
          # 首次初始化订单, hitbtc的orderbook数量很夸张，>>>> 初始化成功，bid数量：267, ask数量： 984 <<<<

          # binding.pry
          # 赋值给实例变量
          bids = this.bid_hash.values.sort[0..49].reverse
          asks = this.ask_hash.values.sort[0..49]

          if bids.any? && asks.any?
            # 赋值给实例变量
            this.bids = bids
            this.asks = asks
            current_time = (Time.now.to_f * 1000).to_i
            this.updated_at = current_time
            res = { data: { bids: bids, asks: asks, time: current_time }, updated_at: current_time }
            this.get_depth = res
            # 执行block
            yield res
          else
            # TODO: ###
          end
        end
      }
    }

  end
end
