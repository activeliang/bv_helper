module Ex
  class << self
    def binance
      ExBinance
    end

    def zb
      ExZb
    end

    def bitfinex
      ExBitfinex
    end

    def huobi
      ExHuobi
    end

    def hitbtc
      ExHitbtc
    end

    def bithumb
      ExBithumb
    end

    def bitmex
      ExBitmex
    end

    def poloniex
      ExPoloniex
    end
  end
end
