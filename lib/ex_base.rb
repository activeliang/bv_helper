class ExBase
  attr_accessor :pair, :ws, :bids, :asks, :updated_at, :client, :get_depth, :exchange_name

  def initialize(pair)
    raise ArgumentError, "没有传递参数pair" unless pair
    @pair = pair.downcase
  end

  def stop
    ws_on_stop
    @ws.close
  end

  def ws_on_stop
    # 断开
    @ws.on :close do |e|
      puts "---------------------->>> 结束 Web Sockets <<<-----------------------"
    end
  end

  def to_hash(array)
    new_array = []
    array.each do |item|
      new_array << { price: item[0], amount: item[1] }
    end
    new_array
  end

  def currency
    pair = @pair
    pair_l = pair.gsub(/eth\z/, '').gsub(/btc\z/, '').upcase
    pair_r = pair.sub(pair_l, '')
    [pair_l, pair_r]
  end

  def exchange_name
    self.class.name.gsub("Ex", '').downcase
  end
end
